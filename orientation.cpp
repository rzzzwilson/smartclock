//********************************************
// A library to handle orientation data from an external sensor.
//********************************************

#include <Wire.h>
#include <MPU6050.h>

#include "smartclock.h"
#include "utility.h"
#include "orientation.h"


// pin the tiltswitch is connected to
static int TiltSwitchPin;


//---------------------------------------------
// Initialize the sensor.
//     pin  pin for the tiltswitch
//---------------------------------------------

void orient_begin(int pin)
{
  TiltSwitchPin = pin;
  pinMode(TiltSwitchPin, INPUT_PULLUP);
}

//---------------------------------------------
// Get the current orientation.
//
// Returns an integer with meaning:
//     0  X is UP
//     1  Y is DOWN
//---------------------------------------------

int orient_read(void)
{
  return digitalRead(TiltSwitchPin);
}
