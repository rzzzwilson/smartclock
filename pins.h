#ifndef _PINS_H_
#define _PINS_H_

/*
 * These define GPIO pins for ESP-12F
 * Program board as Wemos D1 Mini Clone
 */
static const uint8_t GPIO0   = 0;
static const uint8_t GPIO1   = 1;
static const uint8_t GPIO2   = 2;   // TX0
static const uint8_t GPIO3   = 3;   // RX0
static const uint8_t GPIO4   = 4; 
static const uint8_t GPIO5   = 5;
static const uint8_t GPIO6   = 6;
//static const uint8_t GPIO7   = 7;  // reboot
//static const uint8_t GPIO8   = 8;  // reboot 
static const uint8_t GPIO9   = 9;
static const uint8_t GPIO10  = 10;
//static const uint8_t GPIO11  = 11; // reboot
static const uint8_t GPIO12  = 12;
static const uint8_t GPIO13  = 13;
static const uint8_t GPIO14  = 14;
static const uint8_t GPIO15  = 15;
static const uint8_t GPIO16  = 16;

#endif
