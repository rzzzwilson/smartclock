//********************************************
// Interface to a simple library to handle the 4x7 segment display.
//
// This software is designed to work with the smartclock display
// with 4 seven segment LED digits, digit 2 (third from the left)
// being inverted.  The software is low level and uses only the
// SPI library.
//********************************************

#include <SPI.h>

#include "smartclock.h"
#include "display.h"
#include "utility.h"
#include "pins.h"


// SS pin on controller
const int SSPin = GPIO15;

// define 7219 register addresses for setup
const byte DECODE_MODE = 0x09;  // 0x00 = No decode for digits
const byte INTENSITY = 0x0A;    // 0x08 = mid level. Range is 0x00 to 0x0F
const byte SCAN_LIMIT = 0x0B;   // 0x04 for all 4 digits
const byte SHUTDOWN = 0x0C;     // 0x00 - shutdown, 0x01 = normal
const byte DISPLAY_TEST = 0x0F; // 0x00 = normal, 0x01 = display test mode all on full
const byte DIGIT_0 = 0x01;      // address for digit 0
const byte DIGIT_1 = 0x02;      // address for digit 1
const byte DIGIT_2 = 0x03;      // address for digit 2 (upside down)
const byte DIGIT_3 = 0x04;      // address for digit 3 (upside down)

// define bitmasks for the LED segments
const byte LED_DP = 0x80;
const byte LED_SA = 0x40;
const byte LED_SB = 0x20;
const byte LED_SC = 0x10;
const byte LED_SD = 0x08;
const byte LED_SE = 0x04;
const byte LED_SF = 0x02;
const byte LED_SG = 0x01;

// used to "invert" segments for upside down DIGIT_2, DIGIT_3
byte invert_bits[] = {LED_DP, LED_SD, LED_SE, LED_SF,
                      LED_SA, LED_SB, LED_SC, LED_SG};

// "font" data for encoded symbols
const byte LED_D0 = 0b01111110;     // 0
const byte LED_D1 = 0b00110000;     // 1
const byte LED_D2 = 0b01101101;     // 2
const byte LED_D3 = 0b01111001;     // 3
const byte LED_D4 = 0b00110011;     // 4
const byte LED_D5 = 0b01011011;     // 5
const byte LED_D6 = 0b01011111;     // 6
const byte LED_D7 = 0b01110000;     // 7
const byte LED_D8 = 0b01111111;     // 8
const byte LED_D9 = 0b01111011;     // 9
const byte LED_BLANK = 0b00000000;  // blank
const byte LED_DASH = 0b00000001;   // dash
const byte LED_OTA_O = 0b01111110;  // OTA "O"
const byte LED_OTA_TA = 0b01110000; // OTA "T", first half
const byte LED_OTA_TB = 0b01000110; // OTA "T", second half
const byte LED_OTA_A = 0b01110111;  // OTA "A"

byte digits[] = {LED_D0, LED_D1, LED_D2, LED_D3, LED_D4,
                 LED_D5, LED_D6, LED_D7, LED_D8, LED_D9,
                 LED_BLANK, LED_DASH,
                 LED_DP, LED_OTA_O, LED_OTA_TA, LED_OTA_TB, LED_OTA_A};

const byte DIG_BLANK = 10;
const byte DIG_DASH = 11;
const byte DIG_DP = 12;
const byte DIG_OTA_O = 13;
const byte DIG_OTA_TA = 14;
const byte DIG_OTA_TB = 15;
const byte DIG_OTA_A = 16;

//-----------------------------------------------------------
// Define a few "twiddle" sequences.
// For twiddle_seq() an array of struct Sequence values.
//     The (digit, segment) will be displayed in turn.
// For twiddle_par() an array of segment values.
//     Each segment will be displayed in turn on all digits.
//-----------------------------------------------------------

typedef struct
{
  byte digit;
  byte value;
} Sequence;

Sequence config_seq[] = {
                         {DIGIT_0, LED_SA}, {DIGIT_1, LED_SA},
                         {DIGIT_2, LED_SA}, {DIGIT_3, LED_SD},
                         {DIGIT_3, LED_SE}, {DIGIT_3, LED_SF},
                         {DIGIT_3, LED_SA}, {DIGIT_2, LED_SD},
                         {DIGIT_1, LED_SD}, {DIGIT_0, LED_SD},
                         {DIGIT_0, LED_SE}, {DIGIT_0, LED_SF},
                         {0, 0} // to terminate sequence
                        };

//--------------------------------------------------------
// Display orientation
//--------------------------------------------------------

static int Orientation = 0;

//--------------------------------------------------------
// Last displayed bootstage number.
// Needed by the disp_flash_boot_stage() function.
//--------------------------------------------------------

int BootStage = 0;


//---------------------------------------------
// Send SPI "data" to MAX7219 address "addr".
//---------------------------------------------

static void spi_send(byte addr, byte data)
{
  digitalWrite(SSPin, LOW);
  SPI.transfer(addr);
  SPI.transfer(data);
  digitalWrite(SSPin, HIGH);
}

//--------------------------------------------------------
// Flash the stage number display of the boot.
//
// Flashes the LED number stored in the BootStage global.
// disp_boot_stage() sets BootStage.
//--------------------------------------------------------

void disp_flash_boot_stage(void)
{
  for (int i=0; i < 2; ++i)
  {
    disp_clear();
    disp_show();
    delay(50);
    disp_boot_stage(BootStage);
    disp_show();
    delay(100);
  }
}

//---------------------------------------------
// Initialize the display.
//---------------------------------------------

void disp_begin(void)
{
  pinMode(SSPin, OUTPUT);  
  digitalWrite(SSPin, HIGH);

  // turn on SPI port
  SPI.begin();
  spi_send(DISPLAY_TEST, 0x00);   // non-test mode
  spi_send(SHUTDOWN, 0x01);       // display to normal mode
  spi_send(SCAN_LIMIT, 4);        // set display of 4 digits
  spi_send(INTENSITY, 4);         // set average intensity
  disp_clear();                   // clear display
}

//---------------------------------------------
// Show the display in TEST mode, everything ON.
//     pause  the time in milliseconds to show the test
//---------------------------------------------

void disp_test(int pause)
{
  spi_send(DISPLAY_TEST, 0x01);   // test mode
  delay(pause);
  spi_send(DISPLAY_TEST, 0x00);   // non-test mode
}

//---------------------------------------------
// Clear the display.
//---------------------------------------------

void disp_clear(void)
{
  for (int digit = DIGIT_0; digit <= DIGIT_3; ++digit)
  {
    spi_send(digit, LED_BLANK);
  }
}

//---------------------------------------------
// "Invert" a byte of segment data.
//     data  8 bits of segment+dp data
//
// This is done for digits 2 & 3 which are upside down.
// The invert_bits[] array contains bitmasks to select
// the correct bit from the normal segment order.
//---------------------------------------------

static byte invert(byte data)
{
  byte result = 0;

  for (byte i = 0; i < sizeof(invert_bits); ++i)
  {
    result <<= 1;
    if (data & invert_bits[i])
    {
      result |= 1;
    }
  }

  return result;
}

//---------------------------------------------
// Show a time on the display.
//     dig0, dig1, dig2, dig3  the digits to show
//     dp1, dp2, dp3, dp4      turn on DP1, DP2, DP3, DP4
//---------------------------------------------

void disp_digits(byte dig0, byte dig1, byte dig2, byte dig3,
                 bool dp0, bool dp1, bool dp2, bool dp3)
{
  byte show_dp0 = (dp0) ? LED_DP : 0;
  byte show_dp1 = (dp1) ? LED_DP : 0;
  byte show_dp2 = (dp2) ? LED_DP : 0;
  byte show_dp3 = (dp3) ? LED_DP : 0;
  
  if (Orientation)
  {
    spi_send(DIGIT_0, digits[dig0] + show_dp0);
    spi_send(DIGIT_1, digits[dig1] + show_dp1);
    spi_send(DIGIT_2, invert(digits[dig2]) + show_dp2);
    spi_send(DIGIT_3, invert(digits[dig3]) + show_dp3);
  }
  else
  {
    spi_send(DIGIT_3, digits[dig0] + show_dp0);
    spi_send(DIGIT_2, digits[dig1] + show_dp1);
    spi_send(DIGIT_1, invert(digits[dig2]) + show_dp2);
    spi_send(DIGIT_0, invert(digits[dig3]) + show_dp3);
  }
}

//---------------------------------------------
// Show a time on the display.
//     hours, minutes, seconds  the time to show
//
// Twiddle middle DPs every half second.
//---------------------------------------------

void disp_time(int hours, int minutes, int seconds)
{
  byte dig0 = hours / 10;
  byte dig1 = hours % 10;
  byte dig2 = minutes / 10; 
  byte dig3 = minutes % 10;
  byte flash1 = seconds % 2;
  byte flash2 = !flash1;

  disp_digits(dig0, dig1, dig2, dig3, false, flash1, flash2, false);
}

//---------------------------------------------
// Show a date on the display.
//     month, day  the date to show
//
// Leading zeros on day and month are suppressed.
//---------------------------------------------

void disp_date(int month, int day)
{
  byte dig0 = month / 10; 
  byte dig1 = month % 10;
  byte dig2 = day / 10;
  byte dig3 = day % 10;

  if (dig0 == 0)
    dig0 = DIG_BLANK;

  if (dig2 == 0)
    dig2 = DIG_BLANK;

  disp_digits(dig0, dig1, dig2, dig3, false, true, true, false);
}

//---------------------------------------------
// Show the day of the week on the display.
//     day        the date to show
//     sun_first  true if Sunday is first, else Monday
//
// Show digit 4 DP if Sunday is first DoW.
//---------------------------------------------

void disp_dow(int day, bool sun_first)
{
  byte day_digit = day + 1; // assume Sunday is first DoW

  if (!sun_first)
  {
    // Monday is first, raw Sunday (0) is 7, rest unchanged
    day_digit = (day == 0) ? 7 : day;
  }
  
  disp_digits(DIG_BLANK, DIG_BLANK, DIG_BLANK, day_digit,
              false, false, false, sun_first);
}

//---------------------------------------------
// Show a year on the display.
//     year  the date to show
//---------------------------------------------

void disp_year(int year)
{
  byte dig3 = year % 10;
  byte dig2 = (year / 10) % 10; 
  byte dig1 = (year / 100) % 10;
  byte dig0 = (year / 1000) % 10;

  disp_digits(dig0, dig1, dig2, dig3, false, false, false, false);
}

//---------------------------------------------
// Handle a sequence.
//
// Perform sequence "seq" "num" times.
//---------------------------------------------

void twiddle_seq(int num, Sequence *seq, bool erase=true)
{
  disp_clear();

  for (int i = 0; i < num; ++i)
  {
    Sequence *scan = seq;

    while (scan->digit || scan->value)
    {
      if (scan->digit == DIGIT_2)
      {
        spi_send(scan->digit, invert(scan->value));
      }
      else
      {
        spi_send(scan->digit, scan->value);
      }
      delay(20);
      spi_send(scan->digit, LED_BLANK);

      ++scan;
    }
  }
  
  if (erase)
  {
    disp_clear();
  }
}

//---------------------------------------------
// Handle a parallel sequence.
//
// Perform sequence "seq" "num" times.
// The sequnce is performed in parallel on all
// digits.
//---------------------------------------------

void twiddle_par(int num, byte *seq, bool erase=true)
{
  disp_clear();

  for (int i = 0; i < num; ++i)
  {
    byte *scan = seq;
    
    while (*scan)
    {
      spi_send(DIGIT_0, *scan);
      spi_send(DIGIT_1, *scan);
      spi_send(DIGIT_2, invert(*scan));
      spi_send(DIGIT_3, invert(*scan));
      delay(200);
      disp_clear();

      ++scan;
    }
  }

  if (erase)
  {
    disp_clear();
  }
}

//---------------------------------------------
// A function to show boot "state".
//     state  the number of the state to show [1..4]
//
// Turn on 1, 2, 3 or 4 DP segments.
//---------------------------------------------

void disp_boot_stage(int stage)
{
  // remember current boot stage in case we have to flash
  BootStage = stage;

  // first, clear all DP segments
  for (int digit = DIGIT_0; digit <= DIGIT_3; ++digit)
  {
    spi_send(digit, LED_BLANK);
  }

  // now turn on "state" DPs
  for (int i = 0; i < stage; ++i)
  {
    if (Orientation)
    {
      spi_send(DIGIT_0 + i, LED_DP);
    }
    else
    {
      spi_send(DIGIT_3 - i, LED_DP);
    }
  }

  delay(100);    // small delay to show stage clearly
}

//---------------------------------------------
// A function to show clock is in "config" mode.
//---------------------------------------------

void disp_config(void)
{
  disp_clear();
  
  for (int digit = DIGIT_0; digit <= DIGIT_3; ++digit)
  {
    if (digit == DIGIT_2 || digit == DIGIT_3)   // invert digits 2 and 3
    {
      spi_send(digit, invert(LED_SA+LED_SB+LED_SG+LED_SE));
    }
    else
    {
      spi_send(digit, LED_SA+LED_SB+LED_SG+LED_SE);
    }
  }
}

//---------------------------------------------
// Flash a "config" status a number of times.
//     interval  the time to wait between flashes
//---------------------------------------------

void disp_flash_config(unsigned long interval)
{
  static bool showing = false;            // showing the flash?

  if (showing)
  {
    showing = false;
    disp_clear();
  }
  else
  {
    showing = true;
    disp_config();
  }
  
  delay(interval);
}

//---------------------------------------------
// Set the display orientation.
//     orient  the orientation to use
//---------------------------------------------

void disp_orientation(int orient)
{
  Orientation = orient;
}

//---------------------------------------------
//
// Unused in SmartClock.
//---------------------------------------------

void disp_flash_status(int repeat, Status first, Status second)
{
  UNUSED(repeat);
  UNUSED(first);
  UNUSED(second);
}

//---------------------------------------------
// Show some sort of status on the display.
//     status  enum value of the type of status display
//---------------------------------------------

void disp_status(Status status)
{
  switch (status)
  {
    case STATUS_CONFIG_WAIT:
      twiddle_seq(1, config_seq, false);
      break;
    case STATUS_CONFIG:
      spi_send(DIGIT_0, LED_DASH);
      spi_send(DIGIT_1, LED_DASH);
      spi_send(DIGIT_2, LED_DASH);
      spi_send(DIGIT_3, LED_DASH);
      break;
    case STATUS_OTA:
      disp_digits(DIG_OTA_O, DIG_OTA_TA, DIG_OTA_TB, DIG_OTA_A,
                  false, false, false, false);
      break;     
    case STATUS_BOOT:   // ignore all these
    case STATUS_DOW:
    case STATUS_DATE:
    case STATUS_YEAR:
    case STATUS_CLOCK:
    case STATUS_404:
      break;
  }
}

//---------------------------------------------
// Flush any display changes to the hardware.
//
// Unused in SmartClock.
//---------------------------------------------

void disp_show(void)
{
  
}

//---------------------------------------------
// Set the display brightness.
//     bright  the brightness level [0..8]
//
// Actual setting is "bright" + CONFIG brightness.
//---------------------------------------------

void disp_brightness(int bright)
{
  int level = bright + ClockData.Brightness;

// set in smartclock.h
#ifdef REVERSE_BRIGHTNESS
  spi_send(INTENSITY, 8 - level);
#else
  spi_send(INTENSITY, level);
#endif
}

//---------------------------------------------
// Set the clockface to use.
//     face  the face to use
//
// Unused in SmartClock.
//---------------------------------------------

void disp_face(char face)
{
  UNUSED(face);
}

//---------------------------------------------
// Show a debug number on the display.
//     num  the number to show
//---------------------------------------------

void disp_debug(int num, int dots)
{
  num = num & 0xFFFF;
  
  int dig0 = num % 10;
  int tmp = num / 10;
  int dig1 = tmp % 10;
  tmp /= 10;
  int dig2 = tmp % 10;
  tmp /= 10;
  int dig3 = tmp % 10;

  bool dp0 = dots & 0x1;
  bool dp1 = dots & 0x2;
  bool dp2 = dots & 0x4;
  bool dp3 = dots & 0x8;
  
  disp_clear();
  disp_digits(dig3, dig2, dig1, dig0, dp3, dp2, dp1, dp0);
  disp_show();
  delay(1000);
  disp_clear();
  delay(500);
}
