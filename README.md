# A Smart Clock

A traditional 4x7 segment display clock that gets the time from the internet and
automatically handles timezone and daylight saving changes.  Configuration is
done through an Access Point created by the clock itself.  Code update can be
done over-the-air.

Most of the code is copied from the *crypticlock* project.  The code for the
display is different, of course.

## Clock display

When showing the time the display is the traditional HH:MM form and the two
dots in the "colon" alternate to show that the time is being displayed.
If the date display is configured you will see three additional
displays at about the middle of each minute.  These displays are shown below
in the order they will appear: year, date, day of the week:

![year](display_year.png)

The year display is just the four digits of the year in whatever calendar
you have configured.  The display above shows the year as 2566 in the Thai 
Buddhist calendar, which is 2023 in the Gregorian calendar.

![month+day](display_month_day.png)

The date display is the month number first, the *static* colon separator and
then the day number in the month.  The display above show February 17.

![dow](display_dow.png)

The "day of the week" display shows the day number at the rightmost position.
If a dot is shown with the day number this means Sunday is the first day of the
week, number 1.  If no dot is shown then Monday is the first day of the week
and numbered 1.  The display above shows Friday.

## Configuration

When first powered up the clock should display the "configuring" display of
four dashes, possibly after a delay if a previous
WiFi configuration remains.  If you wish to change some configuration values
in a previously configured clock you can force configuration by applying power
while pressing the "Config" button.  This will bring up the "configuring" display:

![configure](display_configure.png)

Now connnect your phone or tablet to the Access Point *SmartClock x.y*:

![connect](connect.png)

and you should see the configuration page:

![configpage](config_english.jpg)

Select the Access Point you want SmartClock to use from the list in the *SSID*
field.  If the AP you want isn't in the list move the SmartClock closer to the 
AP you want to use and again power down/up while holding the "Configure" button
down.

The AP names in the drop-down list should be ordered by signal strength,
strongest at the top.

Once the AP is selected enter a *Password* if one is required to access the AP.

The next field allows you to set the language for the configuration page to
English or Thai.  Any change here only takes effect after a save and reboot.

The *Timezone* field is next, but you don't need to set it.  SmartClock should
guess your timezone from your network's public IP address.  If that doesn't
work, or you want a different timezone from the one you are currently in, enter
your desired timezone.  Timezones have the form *Asia/Bangkok*.  If you don't
know your timezone identifier go to *https://worldtimeapi.org/api/timezone.txt*
and look for your timezone in that page.

Next is the *Reboot hour*.  SmartClock reboots once per day at a configurable
hour.  This reboot checks for the time including any daylight saving in effect.
So you need to set the boot hour to a short time, say one hour, after the hour
daylight savings changes in your locality.

Next, choose the *Calendar* you want to use.

The next two fields control the date display.  You can turn that display off in
the *Show date* field.  If on, the clock will show the year, then month+day, then
day-of-the-week around the middle of each minute.

The *First DoW* field allows you to set Sunday or Monday as the first day of the
week.

The clock will sense the ambient light and dim the display if the room is dark.
The *Brightness* field allows you to vary the overall brightness of the display
a little.

The last two fields (*Debug* and *OTA*) are not important to the end user.

Pressing the "Save" button saves any changes you made and reboots the clock.

### Thai language configuration

In the previous configuration documentation there was a mention of a Thai language
version of the configuration page.  This is the page after setting
the language to Thai:

![configpage_thai](config_thai.jpg)

As I don't read Thai this was produced with the help of Google Translate, so it
could probably use some improvement.

### Problems

If the clock shows garbage on the display when powered up, just remove and reapply
the power.

If you mess up a configuration attempt don't press the save button, just remove
and reapply the power, pressing the "Config" button again if necessary.

If you accidentally select *OTA* and then "Save", the clock will show a
special OTA display and wait for an "Over The Air" upload:

![ota](display_ota.png)

Just remove power and reapply.  Any other changes you made in the other fields
will have been saved.

## Status

The basic clock is finished and in a nice laser-cut acrylic case.

The latest schematic for this version is at *smartclock_schematic.pdf*.
The V2.4 PCB looks like this when populated:

![2.4_PCBs](smartclock_front_PCB.png)

The 7-segment displays, four resistors and the USB-C connector are on the back
of the board.  0805 resistors and capacitors are used with a few exceptions,
like the 470uF capacitor, C5.  A 3mm mercury tilt switch is also used.

This version moves the USB-C socket to the other side of the PCB making the 
acrylic case less fragile.  This means making a small modification to one
7-segment display to clear the socket.

The DXF cut file for the case was designed in KiCad and the file is
*case/case.dxf*.

