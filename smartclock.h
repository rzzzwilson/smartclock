//********************************************
// Common stuff for the SmartClock.
//********************************************

#ifndef _SMARTCLOCK_H_
#define _SMARTCLOCK_H_

#include <Arduino.h>


//--------------------------------------------------------
// If this is defined the brightness sensing is reversed.
//--------------------------------------------------------

//#define REVERSE_BRIGHTNESS

//--------------------------------------------------------
// Set max/min brightness values
//--------------------------------------------------------

const int MinBrightness = 0;    // min/max brightness settings
const int MaxBrightness = 7;

//--------------------------------------------------------
// Status codes, used to show various configurations on display
//--------------------------------------------------------

enum Status {STATUS_BOOT, STATUS_CONFIG, STATUS_CONFIG_WAIT,
             STATUS_DOW, STATUS_DATE, STATUS_YEAR, STATUS_CLOCK,
             STATUS_404, STATUS_OTA};

//--------------------------------------------------------
// Define the ClockData structure.
//--------------------------------------------------------

// set sizes of SSID, Password, URL & NTP/TZ server buffers in EEPROM and RAM
const int SizeCredentialsBuffer = 32;   // limits size of SSID & password
const int SizeURLBuffer = 128;          // size of buffer to create URL
const int SizeTimezoneBuffer = 32;      // buffer for timezone name
const int SizeServerBuffer = 64;        // buffer for TZ server url

// struct holding data saved to EEPROM
typedef struct
{
  char SSID[SizeCredentialsBuffer];         // WiFi SSID name
  char Password[SizeCredentialsBuffer];     // password for the SSID
  char Timezone[SizeTimezoneBuffer];        // timezone to use
  int BootHour;                             // hour when TZ is checked
  bool BootedFlag;                          // 'true' if we are doing hourly boot
  char Calendar;                            // 'G' or 'B' - Gregorian or Buddhist
  bool ShowDate;                            // true if we show date
  bool FirstDoWSunday;                      // true if Sunday is first DoW
  int Brightness;                           // display brightness [0..7]
  bool Debug;                               // true if DEBUG is on
  char Language;                            // 'T' for Thai, 'E' for English
} EepromData;

// in-memory copy of EEPROM data
extern EepromData ClockData;

// macro for comparing strings more naturally
#define STREQ(a, b) (strcmp((a), (b)) == 0)

// macro to turn off warnings about "unused parameter" in functions
#define UNUSED(v) (void) (v)


#endif
