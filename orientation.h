//********************************************
// A library to handle orientation data from an external sensor.
//********************************************

#ifndef _ORIENTATION_H_
#define _ORIENTATION_H_

void orient_begin(int pin);
int orient_read(void);

#endif
