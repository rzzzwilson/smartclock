//********************************************
// A library to sense the ambient light level.
//********************************************

#ifndef _AMBIENT_H_
#define _AMBIENT_H_


void ambient_begin(int pin);
int ambient_read(void);
int ambient_raw(void);


#endif
