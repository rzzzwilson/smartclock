A case for SmartClock
=====================

Use the KiCad PCB editor to design the cutting file.  The actual cutting lines
are drawn in the "Edge.Cuts" layer.  The final DXF file required is created
from the *case.kicad_pcb* file.

A sheet of white paper is inserted in front of the clock display to reduce
brightness a bit.

DXF File
--------

The final case DXF cutting file is produced by doing:

     pcb2dxf.py case.kicad_pcb smartclock_case_glue.dxf

where the file *smartclock_case_glue.dxf* is the new DXF file used to cut the
acrylic sheet.

The *pcb2dxf.py* program was based on the work of Manfred Moitzi.

    # DXF code form the DXF R12 writer by:
    # Copyright (C) 2016, Manfred Moitzi
    # License: MIT License

Plastic Stock
-------------

The case can be cut from 150x200mm sheet (or maybe smaller), 2mm thick,
transparent grey (about 50%).

Screws
------

The screws holding the case together are M1.4x6.
