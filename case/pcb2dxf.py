#!/usr/bin/env python

"""
pcb2dxf.py

A program to convert lines+circles+arcs in a KiCad Edge.Cuts
layer to a DXF format.

Usage: pcb2dxf.py [-d] <PCB filename> <DXF filename>
"""

# DXF code form the DXF R12 writer by:
# Copyright (C) 2016, Manfred Moitzi
# License: MIT License


from contextlib import contextmanager
import os
from math import sqrt, atan2, asin, degrees, acos


TEXT_ALIGN_FLAGS = {'LEFT': (0, 0),
                    'CENTER': (1, 0),
                    'RIGHT': (2, 0),
                    'BOTTOM_LEFT': (0, 1),
                    'BOTTOM_CENTER': (1, 1),
                    'BOTTOM_RIGHT': (2, 1),
                    'MIDDLE_LEFT': (0, 2),
                    'MIDDLE_CENTER': (1, 2),
                    'MIDDLE_RIGHT': (2, 2),
                    'TOP_LEFT': (0, 3),
                    'TOP_CENTER': (1, 3),
                    'TOP_RIGHT': (2, 3),
                   }

@contextmanager
def r12writer(stream, fixed_tables=False):
    if hasattr(stream, 'write'):
        writer = R12FastStreamWriter(stream, fixed_tables)
        yield writer
        writer.close()
    else:
        with open(stream, 'wt') as stream:
            writer = R12FastStreamWriter(stream, fixed_tables)
            yield writer
            writer.close()


class R12FastStreamWriter(object):
    def __init__(self, stream, fixed_tables=False):
        self.stream = stream
        if fixed_tables:
            stream.write(PREFACE)
        stream.write("0\nSECTION\n2\nENTITIES\n")  # write header

    def close(self):
        self.stream.write("0\nENDSEC\n0\nEOF\n")  # write tail

    def add_line(self, start, end, layer="0", color=None, linetype=None):
        debug(f'add_line: {start=}, {end=}, {linetype=}')

        dxf = ["0\nLINE\n"]
        dxf.append(dxf_attribs(layer, color, linetype))
        dxf.append(dxf_vertex(start, code=10))
        dxf.append(dxf_vertex(end, code=11))
        self.stream.write(''.join(dxf))

    def add_circle(self, center, radius, layer="0", color=None, linetype=None):
        debug(f'add_circle: {center=}, {radius=}, {linetype=}')

        dxf = ["0\nCIRCLE\n"]
        dxf.append(dxf_attribs(layer, color, linetype))
        dxf.append(dxf_vertex(center))
        dxf.append(dxf_tag(40, str(rnd(radius))))
        self.stream.write(''.join(dxf))

    def add_arc(self, center, radius, start=0, end=360, layer="0", color=None, linetype=None):
        (center_x, center_y, _) = center

        dxf = ["0\nARC\n"]
        dxf.append(dxf_attribs(layer, color, linetype))
        dxf.append(dxf_vertex(center))
        dxf.append(dxf_tag(40, str(rnd(radius))))
        dxf.append(dxf_tag(50, str(rnd(start))))
        dxf.append(dxf_tag(51, str(rnd(end))))
        self.stream.write(''.join(dxf))

    def add_point(self, location, layer="0", color=None, linetype=None):
        debug(f'add_point: {location=}, {linetype=}')

        dxf = ["0\nPOINT\n"]
        dxf.append(dxf_attribs(layer, color, linetype))
        dxf.append(dxf_vertex(location))
        self.stream.write(''.join(dxf))

    def add_3dface(self, vertices, invisible=0, layer="0", color=None, linetype=None):
        self._add_quadrilateral('3DFACE', vertices, invisible, layer, color, linetype)

    def add_solid(self, vertices, layer="0", color=None, linetype=None):
        self._add_quadrilateral('SOLID', vertices, 0, layer, color, linetype)

    def _add_quadrilateral(self, dxftype, vertices, flags, layer, color, linetype):
        dxf = ["0\n%s\n" % dxftype]
        dxf.append(dxf_attribs(layer, color, linetype))
        vertices = list(vertices)
        if len(vertices) < 3:
            raise ValueError("%s needs 3 ot 4 vertices." % dxftype)
        elif len(vertices) == 3:
            vertices.append(vertices[-1])  # double last vertex
        dxf.extend(dxf_vertex(vertex, code) for code, vertex in enumerate(vertices, start=10))
        if flags:
            dxf.append(dxf_tag(70, str(flags)))
        self.stream.write(''.join(dxf))

    def add_polyline(self, vertices, layer="0", color=None, linetype=None):
        def write_polyline(flags):
            dxf = ["0\nPOLYLINE\n"]
            dxf.append(dxf_attribs(layer, color, linetype))
            dxf.append(dxf_tag(66, "1"))  # entities follow
            dxf.append(dxf_tag(70, flags))
            self.stream.write(''.join(dxf))

        polyline_flags, vertex_flags = None, None
        for vertex in vertices:
            if polyline_flags is None:  # first vertex
                if len(vertex) == 3:  # 3d polyline
                    polyline_flags, vertex_flags = ('8', '32')
                else:  # 2d polyline
                    polyline_flags, vertex_flags = ('0', '0')
                write_polyline(polyline_flags)

            dxf = ["0\nVERTEX\n"]
            dxf.append(dxf_attribs(layer))
            dxf.append(dxf_tag(70, vertex_flags))
            dxf.append(dxf_vertex(vertex))
            self.stream.write(''.join(dxf))
        if polyline_flags is not None:
            self.stream.write("0\nSEQEND\n")

    def add_text(self, text, insert=(0, 0), height=1., width=1., align="LEFT",
                 rotation=0., oblique=0., style='STANDARD',
                 layer="0", color=None):

        # BUG - FIX TEXT
        return

        # text style is always STANDARD without a TABLES section
        dxf = ["0\nTEXT\n"]
        dxf.append(dxf_attribs(layer, color))
        dxf.append(dxf_vertex(insert, code=10))
        dxf.append(dxf_tag(1, str(text)))
        dxf.append(dxf_tag(40, str(rnd(height))))
        if width != 1.:
            dxf.append(dxf_tag(41, str(rnd(width))))
        if rotation != 0.:
            dxf.append(dxf_tag(50, str(rnd(rotation))))
        if oblique != 0.:
            dxf.append(dxf_tag(51, str(rnd(oblique))))
        if style != "STANDARD":
            dxf.append(dxf_tag(7, str(style)))
        halign, valign = TEXT_ALIGN_FLAGS[align.upper()]
        dxf.append(dxf_tag(72, str(halign)))
        dxf.append(dxf_tag(73, str(valign)))
        dxf.append(dxf_vertex(insert, code=11))  # align point
        self.stream.write(''.join(dxf))


def dxf_attribs(layer, color=None, linetype=None):
    dxf = ["8\n%s\n" % layer]  # layer is required
    if linetype is not None:
        dxf.append("6\n%s\n" % linetype)
    if color is not None:
        if 0 <= int(color) < 257:
            dxf.append("62\n%d\n" % color)
        else:
            raise ValueError("color must be an integer in the range [0..256].")
    return "".join(dxf)


def dxf_vertex(vertex, code=10):
    dxf = []
    for c in vertex:
        dxf.append("%d\n%s\n" % (code, str(rnd(c))))
        code += 10
    return "".join(dxf)


def dxf_tag(code, value):
    return "%d\n%s\n" % (code, value)

PREFACE = """  0
SECTION
  2
HEADER
  9
$ACADVER
  1
AC1009
  9
$DWGCODEPAGE
  3
ANSI_1252
  0
ENDSEC
  0
SECTION
  2
TABLES
  0
TABLE
  2
LTYPE
  5
431
 70
20
  0
LTYPE
  5
40F
  2
CONTINUOUS
 70
0
  3
Solid line
 72
65
 73
0
 40
0.0
  0
LTYPE
  5
410
  2
CENTER
 70
0
  3
Center ____ _ ____ _ ____ _ ____ _ ____ _ ____
 72
65
 73
4
 40
2.0
 49
1.25
 49
-0.25
 49
0.25
 49
-0.25
  0
LTYPE
  5
411
  2
DASHED
 70
0
  3
Dashed __ __ __ __ __ __ __ __ __ __ __ __ __ _
 72
65
 73
2
 40
0.75
 49
0.5
 49
-0.25
  0
LTYPE
  5
412
  2
PHANTOM
 70
0
  3
Phantom ______  __  __  ______  __  __  ______
 72
65
 73
6
 40
2.5
 49
1.25
 49
-0.25
 49
0.25
 49
-0.25
 49
0.25
 49
-0.25
  0
LTYPE
  5
413
  2
HIDDEN
 70
0
  3
Hidden __ __ __ __ __ __ __ __ __ __ __ __ __ __
 72
65
 73
2
 40
9.525
 49
6.345
 49
-3.175
  0
LTYPE
  5
43B
  2
CENTERX2
 70
0
  3
Center (2x) ________  __  ________  __  ________
 72
65
 73
4
 40
3.5
 49
2.5
 49
-0.25
 49
0.5
 49
-0.25
  0
LTYPE
  5
43C
  2
CENTER2
 70
0
  3
Center (.5x) ____ _ ____ _ ____ _ ____ _ ____
 72
65
 73
4
 40
1.0
 49
0.625
 49
-0.125
 49
0.125
 49
-0.125
  0
LTYPE
  5
43D
  2
DASHEDX2
 70
0
  3
Dashed (2x) ____  ____  ____  ____  ____  ____
 72
65
 73
2
 40
1.2
 49
1.0
 49
-0.2
  0
LTYPE
  5
43E
  2
DASHED2
 70
0
  3
Dashed (.5x) _ _ _ _ _ _ _ _ _ _ _ _ _ _
 72
65
 73
2
 40
0.3
 49
0.25
 49
-0.05
  0
LTYPE
  5
43F
  2
PHANTOMX2
 70
0
  3
Phantom (2x)____________    ____    ____    ____________
 72
65
 73
6
 40
4.25
 49
2.5
 49
-0.25
 49
0.5
 49
-0.25
 49
0.5
 49
-0.25
  0
LTYPE
  5
440
  2
PHANTOM2
 70
0
  3
Phantom (.5x) ___ _ _ ___ _ _ ___ _ _ ___ _ _ ___
 72
65
 73
6
 40
1.25
 49
0.625
 49
-0.125
 49
0.125
 49
-0.125
 49
0.125
 49
-0.125
  0
LTYPE
  5
441
  2
DASHDOT
 70
0
  3
Dash dot __ . __ . __ . __ . __ . __ . __ . __
 72
65
 73
4
 40
1.4
 49
1.0
 49
-0.2
 49
0.0
 49
-0.2
  0
LTYPE
  5
442
  2
DASHDOTX2
 70
0
  3
Dash dot (2x) ____  .  ____  .  ____  .  ____
 72
65
 73
4
 40
2.4
 49
2.0
 49
-0.2
 49
0.0
 49
-0.2
  0
LTYPE
  5
443
  2
DASHDOT2
 70
0
  3
Dash dot (.5x) _ . _ . _ . _ . _ . _ . _ . _
 72
65
 73
4
 40
0.7
 49
0.5
 49
-0.1
 49
0.0
 49
-0.1
  0
LTYPE
  5
444
  2
DOT
 70
0
  3
Dot .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
 72
65
 73
2
 40
0.2
 49
0.0
 49
-0.2
  0
LTYPE
  5
445
  2
DOTX2
 70
0
  3
Dot (2x) .    .    .    .    .    .    .    .
 72
65
 73
2
 40
0.4
 49
0.0
 49
-0.4
  0
LTYPE
  5
446
  2
DOT2
 70
0
  3
Dot (.5) . . . . . . . . . . . . . . . . . . .
 72
65
 73
2
 40
0.1
 49
0.0
 49
-0.1
  0
LTYPE
  5
447
  2
DIVIDE
 70
0
  3
Divide __ . . __ . . __ . . __ . . __ . . __
 72
65
 73
6
 40
1.6
 49
1.0
 49
-0.2
 49
0.0
 49
-0.2
 49
0.0
 49
-0.2
  0
LTYPE
  5
448
  2
DIVIDEX2
 70
0
  3
Divide (2x) ____  . .  ____  . .  ____  . .  ____
 72
65
 73
6
 40
2.6
 49
2.0
 49
-0.2
 49
0.0
 49
-0.2
 49
0.0
 49
-0.2
  0
LTYPE
  5
449
  2
DIVIDE2
 70
0
  3
Divide(.5x) _ . _ . _ . _ . _ . _ . _ . _
 72
65
 73
6
 40
0.8
 49
0.5
 49
-0.1
 49
0.0
 49
-0.1
 49
0.0
 49
-0.1
  0
ENDTAB
  0
TABLE
  2
STYLE
  5
433
 70
18
  0
STYLE
  5
417
  2
STANDARD
 70
0
 40
0.0
 41
1.0
 50
0.0
 71
0
 42
0.2
  3
txt
  4

  0
STYLE
  5
44A
  2
ARIAL
 70
0
 40
0.0
 41
1.0
 50
0.0
 71
0
 42
1.0
  3
arial.ttf
  4

  0
STYLE
  5
44F
  2
ARIAL_NARROW
 70
0
 40
0.0
 41
1.0
 50
0.0
 71
0
 42
1.0
  3
arialn.ttf
  4

  0
STYLE
  5
453
  2
ISOCPEUR
 70
0
 40
0.0
 41
1.0
 50
0.0
 71
0
 42
1.0
  3
isocpeur.ttf
  4

  0
STYLE
  5
455
  2
TIMES
 70
0
 40
0.0
 41
1.0
 50
0.0
 71
0
 42
1.0
  3
times.ttf
  4

  0
ENDTAB
  0
TABLE
  2
VIEW
  5
434
 70
0
  0
ENDTAB
  0
ENDSEC
"""


def debug(msg):
    if debug_flag:
        print(msg)

def say(msg):
    print(msg)

######
# Routines to handle ARC
######

def calculateH(x1, y1, x2, y2, x3, y3):
    numerator = ((x2*x2+y2*y2)*y3 - (x3*x3+y3*y3)*y2  
                 - ((x1*x1+y1*y1)*y3 - (x3*x3+y3*y3)*y1)
                 + (x1*x1+y1*y1)*y2 - (x2*x2+y2*y2)*y1)
    denominator = ((x2*y3-x3*y2) - (x1*y3-x3*y1) + (x1*y2-x2*y1)) * 2

    return numerator / denominator

def calculateK(x1, y1, x2, y2, x3, y3):
    numerator = (x2*(x3*x3+y3*y3) - x3*(x2*x2+y2*y2)
                 - (x1*(x3*x3+y3*y3) - x3*(x1*x1+y1*y1))
                 + x1*(x2*x2+y2*y2) - x2*(x1*x1+y1*y1))
    denominator = ((x2*y3-x3*y2) - (x1*y3-x3*y1) + (x1*y2-x2*y1)) * 2

    return numerator / denominator

def circle_centre(ax, ay, bx, by, cx, cy):
    x = calculateH(ax, ay, bx, by, cx, cy)
    y = calculateK(ax, ay, bx, by, cx, cy)

    return (x, y)
def rnd(x):  # adjust output precision of floats by changing 'ndigits'
    return round(x, ndigits=6)

def count_parens(line):
    """Returns a count of open/close parens in a line."""

    return line.count("(") - line.count(")")

def next_expr(fd):
    """Return one complete nested line."""

    result = []
    nest = 0
    while True:
        line = fd.readline()
        if not line:
            return None

        line = line.strip()
        parens = count_parens(line)
        nest += parens
        result.append(line)
        if nest == 0:
            break
    return " ".join(result)

def join_expr(in_fd):
    """Read lines from input file, return list of complete expressions."""

    result = []
    while True:
        expr = next_expr(in_fd)
        if expr is None:
            break
        result.append(expr)
    return result

def process_line(expr, dxf):
    """Draw a DXF line."""

    layer = "Edge.Cuts"
    color = 2

    coords = expr.split('(',1)[-1]
    coords = coords.split(" ")
    debug(f'{coords[4]};{coords[5][:-1]} {layer}')
    xs = float(coords[2]);ys = -float(coords[3].split(')')[0])
    xe = float(coords[5]);ye = -float(coords[6].split(')')[0])
    debug(f"Adding line: {(xs,ys)=}, {(xe,ye)=}")
    dxf.add_line((xs,ys), (xe,ye), layer, color, linetype=None)

def process_circle(expr, dxf):
    """Draw a DXF circle."""

    layer = "Edge.Cuts"
    color = 2

    coords = expr.split('(',1)[-1]
    coords = coords.split(" ")
    debug(f'{coords[4]};{coords[5][:-1]}')
    cx = float(coords[2])
    cy = -float(coords[3].split(')')[0])
    xe = float(coords[5])
    ye = -float(coords[6].split(')')[0])
    r = sqrt((cx-xe)**2 + (cy-ye)**2)
    dxf.add_circle((cx, cy), r, layer, color, linetype=None)

def process_arc(expr, dxf):
    """Draw a DXF arc."""

    layer = "Edge.Cuts"
    color = 2

    coords = expr.split('(',1)[-1]
    coords = coords.split(" ")
    start_x = float(coords[2])
    start_y = -float(coords[3].split(')')[0])
    mid_x = float(coords[5])
    mid_y = -float(coords[6].split(')')[0])
    end_x = float(coords[8])
    end_y = -float(coords[9].split(')')[0])

    (centre_x, centre_y) = circle_centre(start_x, start_y, mid_x, mid_y, end_x, end_y)

    r = sqrt((start_x-centre_x)**2+(start_y-centre_y)**2)

    start_angle = degrees(atan2(start_y-centre_y, start_x-centre_x))
    if start_angle < 0:
        start_angle += 360.0

    end_angle = degrees(atan2(end_y-centre_y, end_x-centre_x))
    if end_angle < 0:
        end_angle += 360.0

    center = (centre_x, centre_y, 0)

    dxf.add_arc(center, r, end_angle, start_angle, layer, color, linetype=None)

def process_text(expr, dxf):
    pass

def process_expr(data, out_file):
    """Process expressions in "data"."""

    with r12writer(out_file) as dxf:
        for expr in data:
            if expr.startswith("(gr_line "):
                process_line(expr, dxf)
            if expr.startswith("(gr_circle "):
                process_circle(expr, dxf)
            if expr.startswith("(gr_arc "):
                process_arc(expr, dxf)
            if expr.startswith("(gr_text "):
                process_text(expr, dxf)

def main(in_file, out_file, debug):
    # check that the input file exists
    try:
        in_fd = open(in_file)
    except FileNotFoundError:
        print(f"Input file '{in_file}' doesn't exist.")
        return 1
    except PermissionError:
        print(f"Permissions error: input file '{in_file}' can't be read.")
        return 1

    # prepare the output file
    try:
        out_fd = open(out_file, "w")
    except PermissionError:
        print(f"Permissions error: output file '{out_file}' can't be written.")
        in_fd.close()
        return 1

    # flush the first expression, read input lines, accumulate complete expressions
    header = in_fd.readline()
    data = join_expr(in_fd)

    # now process the expressions
    process_expr(data, out_fd)

    # end, close files
    in_fd.close()
    out_fd.close()

    return 0

##############################################################################

if __name__ == '__main__':
    import sys
    import getopt
    import traceback

    # to help the befuddled user
    def usage(msg=None):
        if msg:
            print(('*'*80 + '\n%s\n' + '*'*80) % msg)
        print(__doc__)

    # our own handler for uncaught exceptions
    def excepthook(type, value, tb):
        msg = '\n' + '=' * 80
        msg += '\nUncaught exception:\n'
        msg += ''.join(traceback.format_exception(type, value, tb))
        msg += '=' * 80 + '\n'
        print(msg)

    # plug our handler into the python system
    sys.excepthook = excepthook

    # parse the CLI params
    argv = sys.argv[1:]

    try:
        (opts, args) = getopt.getopt(argv, 'dh', ['debug', 'help'])
    except getopt.GetoptError as err:
        usage(err)
        sys.exit(1)

    debug_flag = False

    for (opt, param) in opts:
        if opt in ['-d', '--debug']:
            debug_flag = True
        elif opt in ['-h', '--help']:
            usage()
            sys.exit(0)

    # check we have two files
    if len(args) != 2:
        usage(f"Expected two filenames, got {len(args)}")
        sys.exit(1)

    in_file = args[0]
    out_file = args[1]

    # run the program code
    result = main(in_file, out_file, debug_flag)
    sys.exit(result)
